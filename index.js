//BT1
//Input: người dùng nhập 3 số nguyên vào 3 ô input.
//
//Todo:
//- Tạo biến so1, so2, so3 và gán giá trị theo input.
//- in ra console để kiểm tra.
//- tạo biến result và gán giá trị.
//- sử dụng if else kết hợp điều kiện có dạng và, hoặc để giải quyết bài toán
//
// Output: in ra kết quả sắp xếp thứ tự số nguyên từ thông tin người dùng nhập

function sapXepThuTu() {
  var so1 = document.getElementById("txt-so1").value * 1;
  var so2 = document.getElementById("txt-so2").value * 1;
  var so3 = document.getElementById("txt-so3").value * 1;
  //   console.log({ so1, so2, so3 });
  var result = null;
  if (so1 > so2 && so2 > so3) {
    result = `${"Số thứ nhất"} > ${"Số thứ hai"} > ${"Số thứ ba"}`;
  }
  if (so1 > so2 && so2 == so3) {
    result = `${"Số thứ nhất"} > ${"Số thứ hai"} = ${"Số thứ ba"}`;
  }
  if (so1 > so2 && so2 < so3) {
    if (so1 > so3) {
      result = `${"Số thứ nhất"} > ${"Số thứ ba"} > ${"Số thứ hai"}`;
    } else if ((so1 = so3)) {
      result = `${"Số thứ nhất"} = ${"Số thứ ba"} > ${"Số thứ hai"}`;
    } else {
      result = `${"Số thứ ba"} > ${"Số thứ nhất"} > ${"Số thứ hai"}`;
    }
  }

  if (so1 == so2 && so2 > so3) {
    result = `${"Số thứ nhất"} = ${"Số thứ hai"} > ${"Số thứ ba"}`;
  }
  if (so1 == so2 && so2 == so3) {
    result = `${"Số thứ nhất"} = ${"Số thứ hai"} = ${"Số thứ ba"}`;
  }
  if (so1 == so2 && so2 < so3) {
    result = `${"Số thứ nhất"} = ${"Số thứ hai"} < ${"Số thứ ba"}`;
  }

  if (so1 < so2 && so2 > so3) {
    if (so1 > so3) {
      result = `${"Số thứ hai"} > ${"Số thứ một"} > ${"Số thứ ba"}`;
    } else if (so1 == so3) {
      result = `${"Số thứ hai"} > ${"Số thứ một"} = ${"Số thứ ba"}`;
    } else {
      result = `${"Số thứ hai"} > ${"Số thứ ba"} > ${"Số thứ một"}`;
    }
  }
  if (so1 < so2 && so2 == so3) {
    result = `${"Số thứ hai"} = ${"Số thứ ba"} > ${"Số thứ một"}`;
  }
  if (so1 < so2 && so2 < so3) {
    result = `${"Số thứ ba"} > ${"Số thứ hai"} > ${"Số thứ một"}`;
  }
  document.getElementById("result1").innerHTML = `<h1>Thứ tự: ${result}</h1>`;
}

// BT2
//Input: Người dùng select các lựa chọn có sẵn.
//
//Todo:
//- Tạo biến người sử dụng và gán giá trị từ phần người dùng select.
//- tạo biến lời chào và gán giá trị.
//- Sử dụng Swith case để tạo ra lời chào tương ứng với số case - số lựa chọn đã tạo ra trước đó.
//
//Output: đưa ra lời chào tương ứng với lựa chọn của người dùng.

function chaoHoi() {
  var nguoiSuDung = document.getElementById("txt-user").value;
  // console.log("nguoiSuDung: ", nguoiSuDung);
  var loiChao = null;
  switch (nguoiSuDung) {
    case "M":
      loiChao = `${"Chào Mẹ"}`;
      break;
    case "B":
      loiChao = `${"Chào Bố"}`;
      break;
    case "A":
      loiChao = `${"Chào Anh trai"}`;
      break;
    case "E":
      loiChao = `${"Chào Em gái"}`;
      break;
    default:
      loiChao = `${"Chào Người lạ"}`;
  }
  document.getElementById("result2").innerHTML = `<h1>${loiChao}</h1>`;
}

// BT3
//Đếm số lẻ
//Input: Người dùng nhập 3 số nguyên lần lượt vào 3 ô input
//
//Todo:
//- Tạo 3 biến tương đương 3 số nguyên người dùng nhập vào, và gán giá trị dựa trên giá trị lấy từ input của người dùng nhập
//- In ra console 3 số nguyên để kiểm tra
//- Tạo biến đếm số lẻ và gán giá trị
//- Dùng If else để loại trừ các khả năng

//Output: tính ra kết quả tổng số lẻ trong 3 số nguyên người dùng nhập
// Tương tự với đếm số chẵn
function demSoLe() {
  var soNguyen1 = document.getElementById("txt-soNguyen1").value * 1;
  var soNguyen2 = document.getElementById("txt-soNguyen2").value * 1;
  var soNguyen3 = document.getElementById("txt-soNguyen3").value * 1;
  // console.log({ soNguyen1, soNguyen2, soNguyen3 });
  var soLe = null;
  if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 3) {
    soLe = 3;
  } else if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 2) {
    soLe = 2;
  } else if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 1) {
    soLe = 1;
  } else {
    soLe = 0;
  }
  document.getElementById("result3_1").innerHTML = `<h1>${soLe}</h1>`;
}
function demSoChan() {
  var soNguyen1 = document.getElementById("txt-soNguyen1").value * 1;
  var soNguyen2 = document.getElementById("txt-soNguyen2").value * 1;
  var soNguyen3 = document.getElementById("txt-soNguyen3").value * 1;
  // console.log({ soNguyen1, soNguyen2, soNguyen3 });
  var soChan = null;
  if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 0) {
    soChan = 3;
  } else if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 1) {
    soChan = 2;
  } else if ((soNguyen1 % 2) + (soNguyen2 % 2) + (soNguyen3 % 2) == 2) {
    soChan = 1;
  } else {
    soChan = 0;
  }
  document.getElementById("result3_2").innerHTML = `<h1>${soChan}</h1>`;
}

//BT4
//Input: Người dùng nhập giá trị các cạnh của tam giác
//
//Todo:
//- Tạo biến của 3 cạnh và gán giá trị cho 3 cạnh từ giá trị input mà người dùng nhập
//- in console 3 cạnh để kiểm tra
//- Dùng If else để xác định các loại tam giác dựa trên giá trị 3 cạnh
//
//Output: Hiển thị kết quả loại tam giác
function loaiTamGiac() {
  var canh_1 = document.getElementById("txt-canh-1").value * 1;
  var canh_2 = document.getElementById("txt-canh-2").value * 1;
  var canh_3 = document.getElementById("txt-canh-3").value * 1;
  // console.log({ canh_1, canh_2, canh_3 });
  var loaiTamGiac = `${"Tam giác"}`;
  if (canh_1 == canh_2 && canh_2 == canh_3) {
    loaiTamGiac = `${"Tam Giác Đều"}`;
  } else if (canh_1 == canh_2 || canh_1 == canh_3 || canh_2 == canh_3) {
    loaiTamGiac = `${"Tam Giác Cân"}`;
  } else if (
    canh_1 * canh_1 + canh_2 * canh_2 == canh_3 * canh_3 ||
    canh_1 * canh_1 + canh_3 * canh_3 == canh_2 * canh_2 ||
    canh_2 * canh_2 + canh_3 * canh_3 == canh_1 * canh_1
  ) {
    loaiTamGiac = `${"Tam Giác Vuông"}`;
  }
  document.getElementById("result4").innerHTML = `<h1>${loaiTamGiac}</h1>`;
}
